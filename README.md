# README #

This project aims to aggregate small snippets from the web that are usefull in scientific computing. Most of the codes are written in python.

### Installation ###

* Install anaconda form [here](https://www.anaconda.com/distribution/#download-section)
* Install plotly from [here](https://anaconda.org/plotly/plotly)

### Author ###

* Abhinav Gupta