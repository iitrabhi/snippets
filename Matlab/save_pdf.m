function save_pdf(width,height,font_size,file_name)
set(gcf,'DefaultAxesFontName', 'Times New Roman')
set(gca,'FontSize',font_size)
set(gcf,'units','centimeters','position',[0,0,width,height])
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(gcf,file_name,'-dpdf','-painters')
